%global srcname heapdict
%global pkgname HeapDict

Name:           python-%{srcname}
Version:        1.0.0
Release:        2%{?dist}
Summary:        A heap with decrease-key and increase-key operations

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/h/%{srcname}/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
HeapDict is designed to be used as a priority queue, where items are added and \
consumed by priority. Compared to an ordinary dict, a heapdict has the \
following differences: popitem and peekitem returns the (key, priority) pair \
with the lowest priority, instead of a random object.

%description %{_description}

%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{pkgname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
%{__python2} test_heap.py
%{__python3} test_heap.py


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}.py
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg-info
%{python3_sitelib}/__pycache__/%{srcname}*.py?


%changelog
* Tue Aug 22 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-2
- Standardize spec a bit more.
- Simplify description.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-1
- Initial package release.
