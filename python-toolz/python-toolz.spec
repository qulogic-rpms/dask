%global srcname toolz

Name:           python-%{srcname}
Version:        0.8.2
Release:        1%{?dist}
Summary:        List processing tools and functional utilities

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/t/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0001:      %{name}-fix-broken-test.patch

BuildArch:      noarch

%description
A set of utility functions for iterators, functions, and dictionaries.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest

%description -n python2-%{srcname}
A set of utility functions for iterators, functions, and dictionaries.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest

%description -n python3-%{srcname}
A set of utility functions for iterators, functions, and dictionaries.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-2.7 toolz
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} toolz


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/tlz
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/tlz
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sun Feb 26 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.2-1
- Initial package release.
