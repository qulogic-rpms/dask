%global srcname cloudpickle

Name:           python-%{srcname}
Version:        0.3.1
Release:        3%{?dist}
Summary:        Extended pickling support for Python objects

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/c/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0001:      0001-Fix-pickle-with-pytest.patch

BuildArch:      noarch

%description
cloudpickle makes it possible to serialize Python constructs not supported by
the default pickle module from the Python standard library.

cloudpickle is especially useful for cluster computing where Python expressions
are shipped over the network to execute on remote hosts, possibly close to the
data.

Among other things, cloudpickle supports pickling for lambda expressions,
functions and classes defined interactively in the __main__ module.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-mock

%description -n python2-%{srcname}
cloudpickle makes it possible to serialize Python constructs not supported by
the default pickle module from the Python standard library.

cloudpickle is especially useful for cluster computing where Python expressions
are shipped over the network to execute on remote hosts, possibly close to the
data.

Among other things, cloudpickle supports pickling for lambda expressions,
functions and classes defined interactively in the __main__ module.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-mock

%description -n python3-%{srcname}
cloudpickle makes it possible to serialize Python constructs not supported by
the default pickle module from the Python standard library.

cloudpickle is especially useful for cluster computing where Python expressions
are shipped over the network to execute on remote hosts, possibly close to the
data.

Among other things, cloudpickle supports pickling for lambda expressions,
functions and classes defined interactively in the __main__ module.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}:." \
    py.test-2.7 -s
PYTHONPATH="%{buildroot}%{python3_sitelib}:." \
    py.test-%{python3_version} -s


%files -n python2-%{srcname}
%license LICENSE
%doc README.md
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Wed Jul 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-3
- Fix patching error.

* Wed Jul 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-2
- Fix pickling with older pytest.

* Tue Jun 6 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-1
- New upstream release.

* Sun Feb 26 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.2-1
- Initial package release.
