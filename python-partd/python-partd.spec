%global srcname partd

Name:           python-%{srcname}
Version:        0.3.8
Release:        2%{?dist}
Summary:        Appendable key-value storage

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
Key-value byte store with appendable values: Partd stores key-value pairs. \
Values are raw bytes. We append on old values. Partd excels at shuffling \
operations.

%description %{_description}

%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-locket
BuildRequires:  python2-toolz
BuildRequires:  python2-numpy >= 1.9.0
BuildRequires:  python2-pandas
BuildRequires:  python2-zmq
BuildRequires:  python2-blosc

Requires:    python2-locket
Requires:    python2-toolz
Recommends:  python2-numpy >= 1.9.0
Recommends:  python2-pandas
Recommends:  python2-zmq
Recommends:  python2-blosc

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-locket
BuildRequires:  python3-toolz
BuildRequires:  python3-numpy >= 1.9.0
BuildRequires:  python3-pandas
BuildRequires:  python3-zmq
BuildRequires:  python3-blosc

Requires:    python3-locket
Requires:    python3-toolz
Recommends:  python3-numpy >= 1.9.0
Recommends:  python3-pandas
Recommends:  python3-zmq
Recommends:  python3-blosc

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-2.7
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version}


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Tue Aug 22 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.8-2
- Standardize spec a bit more.
- Simplify description a bit.

* Mon Jun 5 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.8-1
- New upstream release.

* Sun Feb 26 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.7-2
- Add missing dependencies.

* Sun Feb 26 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.7-1
- Initial package release.
