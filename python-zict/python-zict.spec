%global srcname zict

Name:           python-%{srcname}
Version:        0.1.2
Release:        3%{?dist}
Summary:        Mutable mapping tools

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/z/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python2-heapdict python3-heapdict
BuildRequires:  python2-lmdb python3-lmdb
BuildRequires:  python2-psutil python3-psutil

%global _description \
Zict builds abstract MutableMapping classes that consume and build on other \
MutableMappings. They can be composed with each other to form intuitive \
interfaces over complex storage systems policies. \
\
Data can be stored in-memory, on disk, in archive files, etc., managed with \
different policies like LRU, and transformed when arriving or departing the \
dictionary.

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-heapdict

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-heapdict

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version}


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Wed Aug 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.2-3
- Enable python-lmdb tests.

* Wed Aug 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.2-2
- Standardize spec a bit more.
- Simplify description a bit.

* Mon Jun 5 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.2-1
- New upstream release.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-1
- Initial package release.
