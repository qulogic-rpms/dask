%global srcname xarray

Name:           python-%{srcname}
Version:        0.9.6
Release:        1%{?dist}
Summary:        N-D labeled arrays and datasets in Python

License:        ASL 2.0
URL:            http://xarray.pydata.org
Source0:        https://files.pythonhosted.org/packages/source/x/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0001:      0001-TST-Skip-rolling-test-with-bottleneck-1.patch
Patch0002:      0002-TST-Fix-tests-against-distributed-1.16.0.patch

BuildArch:      noarch

%description
xarray (formerly xray) is an open source project and Python package that aims
to bring the labeled data power of pandas to the physical sciences, by
providing N-dimensional variants of the core pandas data structures.

Our goal is to provide a pandas-like and pandas-compatible toolkit for
analytics on multi-dimensional arrays, rather than the tabular data for which
pandas excels. Our approach adopts the Common Data Model for self-describing
scientific data in widespread use in the Earth sciences: xarray.Dataset is an
in-memory representation of a netCDF file.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-numpy >= 1.7
BuildRequires:  python2-pandas >= 0.15.0

BuildRequires:  python2-pytest >= 2.7.1
BuildRequires:  python2-dask
BuildRequires:  python2-distributed
BuildRequires:  netcdf4-python
BuildRequires:  python2-h5netcdf

Requires:       python2-numpy >= 1.7
Requires:       python2-pandas >= 0.15.0

%description -n python2-%{srcname}
xarray (formerly xray) is an open source project and Python package that aims
to bring the labeled data power of pandas to the physical sciences, by
providing N-dimensional variants of the core pandas data structures.

Our goal is to provide a pandas-like and pandas-compatible toolkit for
analytics on multi-dimensional arrays, rather than the tabular data for which
pandas excels. Our approach adopts the Common Data Model for self-describing
scientific data in widespread use in the Earth sciences: xarray.Dataset is an
in-memory representation of a netCDF file.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-numpy >= 1.7
BuildRequires:  python3-pandas >= 0.15.0

BuildRequires:  python3-pytest >= 2.7.1
BuildRequires:  python3-dask
BuildRequires:  python3-distributed
BuildRequires:  netcdf4-python3
BuildRequires:  python3-h5netcdf

Requires:       python3-numpy >= 1.7
Requires:       python3-pandas >= 0.15.0

%description -n python3-%{srcname}
xarray (formerly xray) is an open source project and Python package that aims
to bring the labeled data power of pandas to the physical sciences, by
providing N-dimensional variants of the core pandas data structures.

Our goal is to provide a pandas-like and pandas-compatible toolkit for
analytics on multi-dimensional arrays, rather than the tabular data for which
pandas excels. Our approach adopts the Common Data Model for self-describing
scientific data in widespread use in the Earth sciences: xarray.Dataset is an
in-memory representation of a netCDF file.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} --pyargs xarray -ra
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} --pyargs xarray -ra


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Thu Jul 06 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9.6-1
- Update xarray to 0.9.6.

* Mon Jun 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9.5-1
- New upstream release.

* Tue Mar 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9.1-1
- Initial package release.
