%global srcname sortedcollections

# PyPI tarball does not include docs or tests.
%global gituser grantjenks
%global gitname sortedcollections
%global commit 5827683a2733a78cc6cb8c68213a97b7a9cd37c3
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           python-%{srcname}
Version:        0.5.3
Release:        1%{?dist}
Summary:        Python Sorted Collections

License:        ASL 2.0
URL:            https://pypi.python.org/pypi/%{srcname}
#Source0:        https://files.pythonhosted.org/packages/source/s/%%{srcname}/%%{srcname}-%%{version}.tar.gz
Source0:        https://github.com/%{gituser}/%{srcname}/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz

BuildArch:      noarch

%description
SortedCollections is an Apache2 licensed Python sorted collections library:
ValueSortedDict, ItemSortedDict, OrderedDict, OrderedSet, IndexableDict,
IndexableSet, SegmentList.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-nose
BuildRequires:  python2-sortedcontainers

Requires:       python2-sortedcontainers

%description -n python2-%{srcname}
SortedCollections is an Apache2 licensed Python sorted collections library:
ValueSortedDict, ItemSortedDict, OrderedDict, OrderedSet, IndexableDict,
IndexableSet, SegmentList.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-nose
BuildRequires:  python3-sortedcontainers
BuildRequires:  python3-sphinx

Requires:       python3-sortedcontainers

%description -n python3-%{srcname}
SortedCollections is an Apache2 licensed Python sorted collections library:
ValueSortedDict, ItemSortedDict, OrderedDict, OrderedSet, IndexableDict,
IndexableSet, SegmentList.


%prep
%autosetup -n %{srcname}-%{commit}


%build
%py2_build
%py3_build

pushd docs
make SPHINXBUILD=sphinx-build-%{python3_version} html
rm _build/html/.buildinfo
popd


%install
%py2_install
%py3_install


%check
pushd tests
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    nosetests
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    nosetests-%{python3_version}
popd


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst docs/_build/html
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst docs/_build/html
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Tue Jun 6 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.5.3-1
- New upstream release.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.2-2
- Add missing sphinx dependency.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.2-1
- Initial package release.
