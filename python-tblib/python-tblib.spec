%global srcname tblib

Name:           python-%{srcname}
Version:        1.3.2
Release:        2%{?dist}
Summary:        Traceback serialization library

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/t/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
Traceback serialization library that allows you to: \
  * Pickle tracebacks and raise exceptions with pickled tracebacks in different \
    processes. This allows better error handling when running code over \
    multiple processes (imagine multiprocessing, billiard, futures, celery, \
    etc). \
  * Parse traceback strings and raise with the parsed tracebacks.

%description %{_description}

%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-six

Requires:       python2-six

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-six
BuildRequires:  python3-sphinx >= 1.3
BuildRequires:  python3-sphinx-theme-py3doc-enhanced

Requires:       python3-six

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build

PYTHONPATH="src" \
    sphinx-build-%{python3_version} -E -b html docs build/html


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}:tests" \
    py.test -ra tests README.rst
PYTHONPATH="%{buildroot}%{python3_sitelib}:tests" \
    py.test-%{python3_version} -ra tests README.rst


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Wed Aug 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.3.2-2
- Standardize spec a bit more.
- Simplify description a bit.

* Mon Jun 5 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.3.2-1
- New upstream release.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.3.0-1
- Initial package release.
