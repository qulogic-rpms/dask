%global srcname h5netcdf

Name:           python-%{srcname}
Version:        0.3.1
Release:        3%{?dist}
Summary:        netCDF4 via h5py

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/shoyer/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%description
A Python interface for the netCDF4 file-format that reads and writes HDF5 files
API directly via h5py, without relying on the Unidata netCDF library.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-h5py
%if 0%{?fedora} > 25
BuildRequires:  python2-netcdf4
%else
BuildRequires:  netcdf4-python
%endif

BuildRequires:  python2-pytest

Requires:       python2-h5py

%description -n python2-%{srcname}
A Python interface for the netCDF4 file-format that reads and writes HDF5 files
API directly via h5py, without relying on the Unidata netCDF library.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-h5py
%if 0%{?fedora} > 25
BuildRequires:  python3-netcdf4
%else
BuildRequires:  netcdf4-python3
%endif

BuildRequires:  python3-pytest

Requires:       python3-h5py

%description -n python3-%{srcname}
A Python interface for the netCDF4 file-format that reads and writes HDF5 files
API directly via h5py, without relying on the Unidata netCDF library.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} -ra


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Tue Mar 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-3
- Update netCDF dependency for Fedora 26

* Mon Mar 06 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-2
- Tests require netCDF4 Python library as well.

* Mon Mar 06 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-1
- Initial package release.
