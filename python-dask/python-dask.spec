%global srcname dask

Name:           python-%{srcname}
Version:        0.15.1
Release:        1%{?dist}
Summary:        Parallel PyData with Task Scheduling

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/d/%{srcname}/%{srcname}-%{version}.tar.gz
# Not complete.
Patch0001:      0001-TST-Temporarily-XFAIL-test-that-fails-on-32-bit.patch

BuildArch:      noarch

%global _description \
Dask is a flexible parallel computing library for analytic computing, composed \
of two components: \
 1. Dynamic task scheduling optimized for computation. This is similar to \
    Airflow, Luigi, Celery, or Make, but optimized for interactive \
    computational workloads. \
 2. “Big Data” collections like parallel arrays, dataframes, and lists that \
    extend common interfaces like NumPy, Pandas, or Python iterators to \
    larger-than-memory or distributed environments. These parallel collections \
    run on top of the dynamic task schedulers.

%description %{_description}

%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-numpy
BuildRequires:  python2-pandas >= 0.19.0
BuildRequires:  python2-toolz >= 0.7.3
BuildRequires:  python2-cloudpickle >= 0.2.1
BuildRequires:  python2-partd >= 0.3.8
BuildRequires:  python2-psutil
BuildRequires:  python2-h5py
BuildRequires:  python2-scikit-learn
BuildRequires:  python2-scikit-image
BuildRequires:  python2-mock
BuildRequires:  python-backports-lzma

Recommends:  python2-numpy
Recommends:  python2-pandas >= 0.19.0
Recommends:  python2-toolz >= 0.7.3
Recommends:  python2-cloudpickle >= 0.2.1
Recommends:  python2-partd >= 0.3.8

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-numpy
BuildRequires:  python3-pandas >= 0.19.0
BuildRequires:  python3-toolz >= 0.7.3
BuildRequires:  python3-cloudpickle >= 0.2.1
BuildRequires:  python3-partd >= 0.3.8
BuildRequires:  python3-psutil
BuildRequires:  python3-h5py
BuildRequires:  python3-scikit-learn
BuildRequires:  python3-scikit-image

Recommends:  python3-numpy
Recommends:  python3-pandas >= 0.19.0
Recommends:  python3-toolz >= 0.7.3
Recommends:  python3-cloudpickle >= 0.2.1
Recommends:  python3-partd >= 0.3.8

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-2.7 -ra -m "not network" dask
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra -m "not network" dask


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Fri Jul 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.1-1
- Update to latest version.
- Standardize spec a bit more.

* Wed Jul 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.0-2
- Update patches to match upstreamed versions.

* Wed Jul 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.0-1
- Update dask to 0.15.0.

* Tue Feb 28 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-4
- Fix 32-bit build issues.
- Add more test dependencies.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-3
- Skip more tests that use the network.

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-2
- Skip tests that use the network.

* Sun Feb 26 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-1
- Initial package release.
