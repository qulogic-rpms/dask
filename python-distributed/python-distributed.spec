%global srcname distributed

Name:           python-%{srcname}
Version:        1.18.0
Release:        1%{?dist}
Summary:        Distributed computing

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/d/%{srcname}/%{srcname}-%{version}.tar.gz
# Not for upstream:
Patch0001:      0001-TST-Lock-dask-scheduler-to-same-version-as-executabl.patch
Patch0002:      0002-TST-Lock-dask-worker-to-same-version-as-executable.patch
# https://github.com/dask/distributed/pull/1259
Patch0003:      0003-Fix-slow-marker-when-conftest-is-missing.patch

BuildArch:      noarch

%description
Dask.distributed is a lightweight library for distributed computing in Python.
It extends both the concurrent.futures and dask APIs to moderate sized
clusters.


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-six
BuildRequires:  python2-tornado >= 4.4
BuildRequires:  python2-toolz >= 0.7.4
BuildRequires:  python2-msgpack
BuildRequires:  python2-cloudpickle >= 0.2.2
BuildRequires:  python2-dask >= 0.14.1
BuildRequires:  python2-click >= 6.6
BuildRequires:  python2-tblib
BuildRequires:  python2-psutil
BuildRequires:  python2-zict >= 0.1.2
BuildRequires:  python2-sortedcontainers
BuildRequires:  PyYAML
BuildRequires:  python2-futures
BuildRequires:  python2-singledispatch

BuildRequires:  python2-requests
BuildRequires:  python2-mock >= 2.0.0
BuildRequires:  python2-joblib >= 0.10.2
BuildRequires:  python2-lz4
%if 0%{?fedora} > 25
BuildRequires:  python2-netcdf4
%else
BuildRequires:  netcdf4-python
%endif
BuildRequires:  python2-h5py
BuildRequires:  python2-paramiko
BuildRequires:  python2-blosc

BuildRequires:  python2-pandas
BuildRequires:  python2-numpy >= 1.11.0
BuildRequires:  python2-zmq
#BuildRequires:  python2-ipython >= 5.0.0
#BuildRequires:  python2-jupyter_client >= 4.4.0
#BuildRequires:  python2-ipykernel >= 4.5.2

Requires:       python2-six
Requires:       python2-tornado >= 4.4
Requires:       python2-toolz >= 0.7.4
Requires:       python2-msgpack
Requires:       python2-cloudpickle >= 0.2.2
Requires:       python2-dask >= 0.14.1
Requires:       python2-click >= 6.6
Requires:       python2-tblib
Requires:       python2-psutil
Requires:       python2-zict >= 0.1.2
Requires:       python2-sortedcontainers
Recommends:     PyYAML
Recommends:     python2-paramiko
Requires:       python2-futures
Requires:       python2-singledispatch

%description -n python2-%{srcname}
Dask.distributed is a lightweight library for distributed computing in Python.
It extends both the concurrent.futures and dask APIs to moderate sized
clusters.


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-six
BuildRequires:  python3-tornado >= 4.4
BuildRequires:  python3-toolz >= 0.7.4
BuildRequires:  python3-msgpack
BuildRequires:  python3-cloudpickle >= 0.2.2
BuildRequires:  python3-dask >= 0.14.1
BuildRequires:  python3-click >= 6.6
BuildRequires:  python3-tblib
BuildRequires:  python3-psutil
BuildRequires:  python3-zict >= 0.1.2
BuildRequires:  python3-sortedcontainers
BuildRequires:  python3-PyYAML

BuildRequires:  python3-requests
BuildRequires:  python3-mock >= 2.0.0
BuildRequires:  python3-joblib >= 0.10.2
BuildRequires:  python3-lz4
%if 0%{?fedora} > 25
BuildRequires:  python3-netcdf4
%else
BuildRequires:  netcdf4-python3
%endif
BuildRequires:  python3-h5py
BuildRequires:  python3-paramiko
BuildRequires:  python3-blosc

BuildRequires:  python3-pandas
BuildRequires:  python3-numpy >= 1.11.0
BuildRequires:  python3-zmq
#BuildRequires:  python3-ipython >= 5.0.0
#BuildRequires:  python3-jupyter_client >= 4.4.0
#BuildRequires:  python3-ipykernel >= 4.5.2

Requires:       python3-six
Requires:       python3-tornado >= 4.4
Requires:       python3-toolz >= 0.7.4
Requires:       python3-msgpack
Requires:       python3-cloudpickle >= 0.2.2
Requires:       python3-dask >= 0.14.1
Requires:       python3-click >= 6.6
Requires:       python3-tblib
Requires:       python3-psutil
Requires:       python3-zict >= 0.1.2
Requires:       python3-sortedcontainers
Recommends:     python3-PyYAML
Recommends:     python3-paramiko

%description -n python3-%{srcname}
Dask.distributed is a lightweight library for distributed computing in Python.
It extends both the concurrent.futures and dask APIs to moderate sized
clusters.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py3_install
for name in remote scheduler ssh submit worker; do
mv %{buildroot}%{_bindir}/dask-${name} %{buildroot}%{_bindir}/dask-${name}-%{python3_version}
ln -s dask-${name}-%{python3_version} %{buildroot}%{_bindir}/dask-${name}-3
done
%py2_install
for name in remote scheduler ssh submit worker; do
mv %{buildroot}%{_bindir}/dask-${name} %{buildroot}%{_bindir}/dask-${name}-%{python2_version}
ln -s dask-${name}-%{python2_version} %{buildroot}%{_bindir}/dask-${name}-2
ln -s dask-${name}-2 %{buildroot}%{_bindir}/dask-${name}
done
rm %{buildroot}%{python2_sitelib}/%{srcname}/asyncio.py
rm %{buildroot}%{python2_sitelib}/%{srcname}/tests/py3_test_*.py


%check
PATH="%{buildroot}%{_bindir}:$PATH" PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-2.7 -ra -m "not ipython and not avoid_travis" distributed
LANG=C.UTF-8 PATH="%{buildroot}%{_bindir}:$PATH" PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra -m "not ipython and not avoid_travis" distributed


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{_bindir}/dask-remote
%{_bindir}/dask-scheduler
%{_bindir}/dask-ssh
%{_bindir}/dask-submit
%{_bindir}/dask-worker
%{_bindir}/dask-remote-2
%{_bindir}/dask-scheduler-2
%{_bindir}/dask-ssh-2
%{_bindir}/dask-submit-2
%{_bindir}/dask-worker-2
%{_bindir}/dask-remote-2.7
%{_bindir}/dask-scheduler-2.7
%{_bindir}/dask-ssh-2.7
%{_bindir}/dask-submit-2.7
%{_bindir}/dask-worker-2.7
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{_bindir}/dask-remote-3
%{_bindir}/dask-scheduler-3
%{_bindir}/dask-ssh-3
%{_bindir}/dask-submit-3
%{_bindir}/dask-worker-3
%{_bindir}/dask-remote-%{python3_version}
%{_bindir}/dask-scheduler-%{python3_version}
%{_bindir}/dask-ssh-%{python3_version}
%{_bindir}/dask-submit-%{python3_version}
%{_bindir}/dask-worker-%{python3_version}
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Fri Jul 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.18.0-1
- Update to latest release.
- Standardize spec a bit more.
- Add patch to fix slow marker.

* Fri Jul 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.17.1-1
- Update distributed to 1.17.1.
- Update pytest 2.9 compatibility patch.
- Skip bokeh requiring test.
- Add PEM files to manifest.
- Add missing test TLS files.
- Fix PYTHONPATH problems.
- Add paramiko and PyYAML requirements.
- Skip memoryview tests on Python 2.

* Wed Mar 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.16.0-4
- Patch 32-bit test failures on Python 2 also

* Tue Mar 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.16.0-3
- Patch 32-bit test failures

* Tue Mar 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.16.0-2
- Update netCDF dependency for Fedora 26

* Mon Feb 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.16.0-1
- Initial package release.
